//Activity - s24  "MongoDB - Query Operators and Field Projection"


		//sol 2
		db.users.find({
			$or: [
				{"firstName": {$regex: "s",$options: "$i"}},
				{"lastName": {$regex: "d",$options: "$i"}}
			]
		},
		{
			"firstName":1,
			"lastName":1,
			"_id":0
		})

		//sol 3
		db.users.find({
		    $and: [
		        {"department": {$regex: "HR",$options: "$i"}},
				{"age": {$gte:70}}
			]
		})

		//sol 4
		db.users.find({
			$and: [
				{"firstName": {$regex: "e",$options: "$i"}},
				{"age": {$lte: 30}}
			]
		})
